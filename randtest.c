#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <math.h>
#include <mkl.h>
#include <time.h>
#include "c_timer.h"
#include "matrixUtils.h"

int main(int argc, char** argv){
	if(argc != 3){
		printf("\n\tUsage: %s <numberOfRandomTests> <tolerance>\n\n",argv[0]);
		exit(1);
	}
	
	int numberOfTests = atoi(argv[1]);
	bool normPassed = true, matVecPassed = true, matMatPassed = true;
	double tol = atof(argv[2]), error;
	srand(time(NULL));
	for(int q = 0; q < numberOfTests; q++){
		int m = rand()%990+10, n = rand()%990+10, k = rand()%990+10;
		double *x = (double*)malloc(n*sizeof(double)),
			*yActual = (double*)malloc(m*sizeof(double)), 
			*yComputed = (double*)malloc(m*sizeof(double)); 
		printf("m: %d,\tn: %d,\tk: %d\n",m,n,k);
		for(int i = 0; i < n; i++){
			x[i] = randInRange(-1,1);	
		}
		for(int i = 0; i < m; i++){
			yActual[i] = randInRange(-1,1);
			yComputed[i] = yActual[i];
		}
		error = cblas_dnrm2(n,x,1);
		error = fabs(error-d2norm(x,n))/error;
		printf("Relative error in my 2-norm: %e\n",error);
		normPassed &= (error < tol);
		double* A =(double*)malloc(m*n*sizeof(double));
		for(int i = 0; i < m*n; i++)
			A[i] = randInRange(-1,1);
		dAx(A,x,yComputed,m,n);
		cblas_dgemv(CblasRowMajor,
					 CblasNoTrans, m, n,
					 1.0, A, n,
					 x, 1, 1.0,
					 yActual, 1);	
		error = cblas_dnrm2(m,yActual,1);
		cblas_daxpy(m, -1., yComputed, 1., yActual, 1.);
		error = cblas_dnrm2(m,yActual,1)/error;
		printf("Relative Error of Matrix-Vector Product:\t%e\n",
			error);
		matVecPassed &= (error<tol);
		free(A);
		A = (double*)malloc(m*k*sizeof(double));
		double* B = (double*)malloc(k*n*sizeof(double));
		double* actualC = (double*)calloc(m*n,sizeof(double));
		double* computedC = (double*)calloc(m*n,sizeof(double));
		for(int i = 0; i < m*k; i++)
			A[i] = randInRange(-1,1);
		for(int i = 0; i < k*n; i++)
			B[i] = randInRange(-1,1);
		for(int i = 0; i < m*n; i++){
			actualC[i] = randInRange(-1,1);
			computedC[i] = actualC[i];
		}

		dAB(A,B,computedC,m,k,n);
		cblas_dgemm(CblasRowMajor,CblasNoTrans, CblasNoTrans, m, n , k , 1.0, A, k, B, n, 1.0, actualC, n);
		cblas_dgemv(CblasRowMajor,
					 CblasNoTrans, m, n,
					 1.0, actualC, n,
					 x, 1, 0.0,
					 yActual, 1);	
		cblas_dgemv(CblasRowMajor,
					 CblasNoTrans, m, n,
					 1.0, computedC, n,
					 x, 1, 0.0,
					 yComputed, 1);	
		error = cblas_dnrm2(m,yActual,1);
		cblas_daxpy(m, -1., yComputed, 1., yActual, 1.);
		error = cblas_dnrm2(m,yActual,1)/error;
		printf("Relative Error of Product ABx:\t%e\n", error);
		matMatPassed &= (error<tol);
		free(A);
		free(B);
		free(computedC);
		free(actualC);
		free(x);
		free(yActual);
		free(yComputed);
	}
	printf("With tolerance:\t%e\n",tol);
	printf("2-norm passed:\t%s\nMat.-Vec. Mult. Passed:\t%s\nMat.-Mat. Mult. Passed:\t%s\n",
		   normPassed?"true":"false",matVecPassed?"true":"false",matMatPassed?"true":"false");
	return 0;
}
