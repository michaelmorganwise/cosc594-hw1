figure;
hold on;
title('Rate of Execution vs. Problem Size (Dell)');
xlabel('Problem Size (n)');
ylabel('Rate of Execution (GFLOPs/sec)');
theoreticalPeak = 12;
load d2nrm.dat; x = d2nrm(:,1); y= d2nrm(:,2); plot(x,y/1.0e9);
disp(['peak for 2nrm : ',num2str((max(y)/1e9)*100/theoreticalPeak),'%'])
load dAxpy.dat; x = dAxpy(:,1); y= dAxpy(:,2); plot(x,y/1.0e9);
disp(['peak for dAxpy : ',num2str((max(y)/1e9)*100/theoreticalPeak),'%'])
load dABpC.dat; x = dABpC(:,1); y= dABpC(:,2); plot(x,y/1.0e9);
disp(['peak for dABpC : ',num2str((max(y)/1e9)*100/theoreticalPeak),'%'])
y=theoreticalPeak*ones(size(x)); plot(x,y);
legend('2-norm','y=y+Ax','C=C+AB');
axis([0,1000,0,2.5])