#ifndef MATRIXUTILS_H 
#define MATRIXUTILS_H

#include <stdlib.h>

double randInRange(double a, double b);
double d2norm(double* x, int n);
void dAx(double* A, double* x, double* b, int m, int n);
void dAB(double* A, double* B, double* C, int m, int k, int n);
void printVector(double* x, long n);
void printMatrix(double* A, long m, long n);

#endif
