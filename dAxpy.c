#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "c_timer.h"
#include "matrixUtils.h"

int main(int argc, char **argv)
{
	if(argc != 2){
		printf("\n\tUsage: %s <n>\n\n",argv[0]);
		exit(1);
	}
	srand(time(NULL));
	int n = atoi(argv[1]);
	double start, end;
	
	double* A =(double*)malloc(n*n*sizeof(double));
	for(int i = 0; i < n*n; i++)
		A[i] = randInRange(-1,1);
	
	double *x = (double*)malloc(n*sizeof(double));
	double *y = (double*)malloc(n*sizeof(double));
	for(int i = 0; i < n; i++){
		x[i] = randInRange(-1,1);
		y[i] = randInRange(-1,1);
	}
	
	start = get_cur_time();
	for(int i = 0; i < 5; i++)
		dAx(A,x,y,n,n);
	end = get_cur_time();
	
	printf("%d,\t%f\n",n,5.0*(2.0*n*n)/(end-start));
	
	free(A);
	free(x);
	free(y);
}
