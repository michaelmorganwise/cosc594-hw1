#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "c_timer.h"
#include "matrixUtils.h"

int main(int argc, char **argv)
{
	if(argc != 2){
		printf("\n\tUsage: %s <n>\n\n",argv[0]);
		exit(1);
	}
	srand(time(NULL));
	int n = atoi(argv[1]);
	double start, end;
	
	double *x = (double*)malloc(n*sizeof(double));
	for(int i = 0; i < n; i++)
		x[i] = randInRange(-1,1);
	
	start = get_cur_time();
	for(int i = 0; i < 5; i++)
		d2norm(x,n);
	end = get_cur_time();
	
	printf("%d,\t%f\n",n,5.0*(2.0*n)/(end-start));
	
	free(x);
}
