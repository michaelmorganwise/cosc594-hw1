#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <math.h>
#include <mkl.h>
#include <time.h>
#include "c_timer.h"
#include "matrixUtils.h"

int main(int argc, char** argv){
	if(argc != 3){
		printf("\n\tUsage: %s <maximum n> <tolerance>\n\n",argv[0]);
		exit(1);
	}
	int maxDim = atoi(argv[1]);
	bool normPassed = true, matVecPassed = true, matMatPassed = true;
	double tol = atof(argv[2]), error;
	for(int n = 1; n <= maxDim; n++){
		printf("\nn = %d\n",n);
		double *x = (double*)malloc(n*sizeof(double)),
		*yActual = (double*)malloc(n*sizeof(double)),
		*yComputed = (double*)malloc(n*sizeof(double));
		for(int i = 0; i < n; i++){
			x[i] = randInRange(-1,1);
		}
		for(int i = 0; i < n; i++){
			yActual[i] = randInRange(-1,1);
			yComputed[i] = yActual[i];
		}
		error = cblas_dnrm2(n,x,1);
		error = fabs(error-d2norm(x,n))/error;
		printf("Relative error in my 2-norm: %e\n",error);
		normPassed &= (error < tol);
		double* A =(double*)malloc(n*n*sizeof(double));
		for(int i = 0; i < n*n; i++)
			A[i] = randInRange(-1,1);
		dAx(A,x,yComputed,n,n);
		cblas_dgemv(CblasRowMajor,
					CblasNoTrans, n, n,
					1.0, A, n,
					x, 1, 1.0,
					yActual, 1);
		error = cblas_dnrm2(n,yActual,1);
		cblas_daxpy(n, -1., yComputed, 1., yActual, 1.);
		error = cblas_dnrm2(n,yActual,1)/error;
		printf("Relative Error of Matrix-Vector Product:\t%e\n",
			   error);
		matVecPassed &= (error<tol);
		
		free(A);
		A = (double*)malloc(n*n*sizeof(double));
		double* B = (double*)malloc(n*n*sizeof(double));
		double* actualC = (double*)malloc(n*n*sizeof(double));
		double* computedC = (double*)malloc(n*n*sizeof(double));
		for(int i = 0; i < n*n; i++)
			A[i] = randInRange(-1,1);
		for(int i = 0; i < n*n; i++)
			B[i] = randInRange(-1,1);
		for(int i = 0; i < n*n; i++){
			actualC[i] = randInRange(-1,1);
			computedC[i] = actualC[i];
		}
		
		dAB(A,B,computedC,n,n,n);
		cblas_dgemm(CblasRowMajor,CblasNoTrans, CblasNoTrans, n,n,n , 1.0, A, n, B, n, 1.0, actualC, n);
		cblas_dgemv(CblasRowMajor,
					CblasNoTrans, n, n,
					1.0, actualC, n,
					x, 1, 0.0,
					yActual, 1);
		cblas_dgemv(CblasRowMajor,
					CblasNoTrans, n, n,
					1.0, computedC, n,
					x, 1, 0.0,
					yComputed, 1);
		error = cblas_dnrm2(n,yActual,1);
		cblas_daxpy(n, -1., yComputed, 1., yActual, 1.);
		error = cblas_dnrm2(n,yActual,1)/error;
		printf("Relative error of Product ABx:\t%e\n", error);
		matMatPassed &= (error<tol);
		
		free(A);
		free(B);
		free(computedC);
		free(actualC);
		free(x);
		free(yActual);
		free(yComputed);
	}
	printf("\n\nWith tolerance:\t%e\n",tol);
	printf("2-norm passed:\t%s\nMat.-Vec. Mult. Passed:\t%s\nMat.-Mat. Mult. Passed:\t%s\n",
		   normPassed?"true":"false",matVecPassed?"true":"false",matMatPassed?"true":"false");
	return 0;
}
