\documentclass[12pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{float}
\usepackage{mathpazo}
\usepackage{listings}
\usepackage{url}
\lstdefinestyle{customc}{
  language=C,
  basicstyle=\footnotesize\ttfamily,
}
\usepackage{amsmath,amsthm,amssymb}
\usepackage{graphicx}
%\usepackage{times}

\theoremstyle{theorem}
\newtheorem{theorem}{Theorem}

\theoremstyle{definition}
\newtheorem{definition}{Definition}
\newtheorem*{remark}{Remark}
\newtheorem*{note}{Note}

\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\pd}[3]{\frac{\partial^{#3}{#1}}{\partial{#2}^{#3}}}

\setlength{\parindent}{4em}
\setlength{\parskip}{1em}
\renewcommand{\baselinestretch}{1.0}

\begin{document}
\title{COSC 594: Homework \#1 Report}

\author{Michael Morgan Wise}
\date{February 3, 2016}
\maketitle
\section{Introduction}
In the following, a brief
performance analysis and discussion of the 
implementation of the following
operations will be given:
\begin{itemize}
	\item the 2-norm of a vector, i.e.,
	\begin{equation}
		\|x\|_2=\sqrt{x^Tx}=\sqrt{\sum_{i=1}^n{x_i\cdot x_i}}	
	\end{equation}
	\item matrix-vector multiplication, i.e.,
	\begin{align}
		y&=y+Ax\\
		y_i&=y_i+\sum_{j=1}^{n}{A_{i,j}\cdot x_{j}},\quad
		\text{for $i,j=1,\ldots,n$.}	
	\end{align}
	\item matrix multiplication
	\begin{align}
		C&=C+AB\\
		C_{i,j}&=C_{i,j}+\sum_{k=1}^n{A_{i,k}\cdot B_{k,j}},
		\quad\text{for }i,j=1,\ldots,n.
	\end{align}
\end{itemize}
The verification of these implementations against the BLAS (included
in the Intel MKL) will be discussed.
Plots of performance data regarding the rate of execution
on a Macbook Pro Laptop (Intel Core
i7-620M CPU @ 2.67GHz, 8 GB Memory),
Dell Studio Desktop (Intel Core
2 Quad CPU @ 3.0GHz, 4 GB Memory), and on a compute node of 
a Cray XC30 (Cascade) (Two 2.6 GHz 64 bit Intel 8-core 
Xeon E5-2670 (Sandy Bridge), 32 GB of memory) will be given. Concluding
remarks will be made regarding the peak performance of these machines,
the very different performance rates using the implementations discussed, 
and suggestions to improve the performance rate of the implementations.
\section{Implementation}
Note: the project is available on at the following git 
repository:
\begin{center}
\url{https://bitbucket.org/michaelmorganwise/cosc594-hw1}.
\end{center}
\subsection{Language and Compilers Used}
Implementation was carried out in the C programming language, using current
standard C11. On the MacBook Pro, the default C compiler packaged with 
XCode was utilized (Apple LLVM version 7.0.2 (clang-700.1.81)).
On the Dell Studio with Fedora 21, the GNU gcc compiler 
compiler (gcc version 4.9.2 20150212 Red Hat 4.9.2-6) 
was used. 
On Darter, the Intel Compiler (icc version 14.0.2 
(gcc version 4.3.0 compatibility)) was used. 
\subsection{Implementation Details}
As requested, serial na\"ive 2-norm, matrix-vector multiply, and matrix-matrix
multiply were implemented in the functions \texttt{double d2norm($\ldots$)},
\texttt{void dAxpy($\ldots$)}, and \texttt{void dABpC($\ldots$)}. 
Source is included in the figures below.
\lstset{style=customc}
\begin{figure}[H]
\begin{lstlisting}
double d2norm(double* x, int n){
	double sum = 0;
	for(int i = 0;i < n; ++i){
		sum += x[i]*x[i];
	}
	return sqrt(sum);	
}
\end{lstlisting}
\caption{Na\"ive 2-norm Implementation}
\end{figure}
\begin{figure}[H]
\begin{lstlisting}
void dAxpy(double* A, double* x, double* b, int m, int n){
	int j;
	for(int i = 0; i < m; ++i){
		for(j = 0; j < n; ++j){
			b[i] += A[i*n+j]*x[j];
		}
	}	
}
\end{lstlisting}
\caption{Na\"ive Matrix-Vector Multiply Implementation}
\end{figure}
\begin{figure}[H]
\begin{lstlisting}
void dABpC(double* A, double* B, double* C, int m, int k, int n){
	int j, l;
	double sum = 0;
	for(int i = 0; i < m; ++i){
		for(j = 0; j < n; ++j){
			sum = 0;
			for(l = 0; l < k; ++l){
				sum += A[i*k+l]*B[l*n+j];
			}
			C[i*n+j] += sum;
		}
	}
}
\end{lstlisting}
\caption{Na\"ive Matrix-Matrix Multiply Implementation}
\end{figure}
\subsection{Testing}
In order to validate these functions, two test programs were written, 
\texttt{ntest} and \texttt{randtest}. Their source is included in the
repository. A brief description of each follows:
\begin{itemize}
	\item \textbf{\texttt{ntest}} --- This program reads in 
	an integer $n$ and a tolerance $tol$. The integer $n$ serves
	as the maximum dimension with which each of the functions described above
	will be tested. For each integer $k\in\{1\ldots n\}$,
	the procedure below is followed. 
	A pseudorandom vector $x\in\R^n$ and three pseudorandom
	matrices $A,B,C\in\R^{n\times n}$ are generated. 
	Values are real values from the interval $[-1,1]$. 
	$\|x\|_2$, $y=y+Ax$, and $C=C+AB$ are computed using the functions
	described above and the BLAS functions from the MKL. 
	\textbf{Note:} To test $C=C+AB$, the matrix $\tilde{C}$ computed with the BLAS
	and the matrix $C$ computed with the function described above
	are each multiplied by a pseudorandom test vector $x\in\R^n$.
	Relative errors for each operation are computed and verified against
	the tolerance $tol$. Once the computations have finished for each 
	$k\in\{1\ldots n\}$, the program notifies
	the user of the success or failure of the tests.  
	\item \textbf{\texttt{randtest}} --- This program reads in 
	an integer $maxIter$ and a tolerance $tol$. The integer $maxIter$ serves
	as the maximum number of iterations 
	with which each of the functions described above
	will be tested. In each test, pseudorandom matrix dimensions 
	$m,k,n\in\{10,\ldots,1000\}$ are selected.
	Necessary pseudorandom vectors and matrices are 
	generated with real values from the interval $[-1,1]$. 
	$\|x\|_2$ (for $x\in\R^n$), $y=y+Ax$ ($A\in\R^{m\times n}$, $x\in\R^n$, $y\in\R^m$), and $C=C+AB$ ($A\in\R^{m\times k}$, $B\in\R^{k\times n}$, $C\in\R^{m\times n}$) are computed using the functions
	described above and the BLAS functions from the MKL. 
	To test $C=C+AB$, the matrix $\tilde{C}$ computed with the BLAS
	and the matrix $C$ computed with the function described above
	are each multiplied by a pseudorandom test vector $x\in\R^n$.
	Relative errors for each operation are computed and verified 
	against tolerance $tol$. This test is conducted for $maxIter$ 
	times. Once
	finished, the program notifies 
	the user if any iterations failed the test.   
\end{itemize} 
\texttt{ntest} and \texttt{randtest} were executed many times
and succeeded even with $n,maxIter=1000$ and $tol=1.0\times 10^{-14}$.
\section{Performance Results}
The computational functions verified above were called in 
corresponding C programs and 
were tested with pseudorandom values from $[-1,1]$ and for matrices 
$A,B,C\in\R^{n\times n}$ and vectors $x\in\R^n$, with
$n\in\{10,20,30,\ldots, 1000\}$. The C timer included with the assignment link
was used. Due to the problem's simplicity, timing was 
difficult for \texttt{double d2norm($\ldots$)}, performance rate
data was collected
from 50 repetitions conducted for each $n$. 
Along the same lines but yielding to time constraints, performance 
rate data was collected for \texttt{void dAxpy($\ldots$)} and 
\texttt{void dABpC($\ldots$)} timing 5 repetitions for each $n$ for each 
function. Data was collected on several machines which shall be discussed
below.
\subsection{Results on Macbook Pro}
First, data was collected 
Macbook Pro Laptop running OS X 10.11.1 (El Capitan) 
with an Intel Core
i7-620M CPU @ 2.67GHz and 8 GB Memory. From information shared via e-mail
by Mr. Eberius, 4 double precision FLOPs/cycle can be 
carried out on this processor, implying the following:
\begin{align*}
	\text{Theoretical Peak Performance (Single Core)}=&\text{ 2.67 GHz (CPU Speed) }\times\\
	&\text{ 4 (FLOPs/cycle)}\\
	=&\text{ 10.68 GFLOPS}.
\end{align*}
Note that the theoretical peak performance for the machine can 
be found by multiplying this number by the number of cores.
A plot of the performance rate of each of the functions above on this
machine is given below. For more information on this CPU, please see
\begin{center}
\url{http://www.cpu-world.com/CPUs/Core_i7/Intel-Core%20i7%20Mobile%20I7-620M%20CP80617003981AH.html}.	
\end{center}

\begin{figure}[H]
	\includegraphics[width=1.0\textwidth]{mac-output/macPerformance}
\end{figure}
\noindent Note that the maximum performance achieved in this run
with the 2-norm 
is roughly 39\% of the theoretical peak performance.
With $y=y+Ax$, only around 21\% of the 
theoretical peak was ever obtained.
Finally, with $C=C+AB$, the rate only reached only 18\% of
the peak.
\subsection{Results on Dell Studio}
Data was also collected on a dated 
Dell Studio Desktop running Fedora 21 with an Intel Core
2 Quad CPU Q9650 @ 3.0GHz and 4 GB Memory. 
By the information discussed earlier,
4 FLOPs/cycle can be carried out on this processor, implying the following:
\begin{align*}
	\text{Theoretical Peak Performance (Single Core)}=&\text{ 3.0 GHz (CPU Speed) }\times\\
	&\text{ 4 (FLOPs/cycle)}\\
	=&\text{ 12 GFLOPS}.
\end{align*}
A plot of the performance rate of each of the functions above on this
machine is given below. For more information on this CPU, please see
\begin{center}
\url{http://www.cpu-world.com/CPUs/Core_2/Intel-Core%202%20Quad%20Q9650%20AT80569PJ080N%20(BX80569Q9650%20-%20BXC80569Q9650).html}.	
\end{center}
\begin{figure}[H]
	\centering
	\includegraphics[width=1.0\textwidth]{fedora-output/dellPerformance}
\end{figure}
\noindent Observe that the maximum performance achieved in this run
with the 2-norm 
is only roughly 17\% of the theoretical peak performance.
With $y=y+Ax$, also around 17\% of the 
theoretical peak was obtained.
Finally, with $C=C+AB$, only 15\% of the 
theoretical peak performance was ever reached.
\subsection{Results on Darter}
Finally, the implementations were tested on 
Darter, a Cray XC30 (Cascade) Compute Node with two 2.6 GHz 64 bit Intel 8-core 
Xeon E5-2670 (Sandy Bridge) and 32 GB of memory.
Based on the information from Mr. Eberius, theoretical peak performance for 
a single core may be computed as follows:  
\begin{align*}
	\text{Theoretical Peak Performance (Single Core)}=&\text{ 2.6 GHz (CPU Speed) }\times\\
	&\text{ 8 (FLOPs/cycle)}\\
	=&\text{ 20.8 GFLOPS}.
\end{align*}  
A plot of the performance rate of each of the functions above on this
machine is given below. For more information on this CPU, please see
\begin{center}
\url{http://www.cpu-world.com/CPUs/Xeon/Intel-Xeon%20E5-2670.html#specs}.	
\end{center}
\begin{figure}[H]
	\centering
	\includegraphics[width=1.0\textwidth]{darter-output/darterPerformance}
\end{figure}
\noindent Finally, note that the maximum performance achieved 
in this run
with the 2-norm 
is only roughly 48\% of the theoretical peak performance.
With $y=y+Ax$, the implementation achieved 
about 29\% of the theoretical peak.
Finally, with $C=C+AB$, the maximum performance was only 20\%
of the theoretical peak, which decreased quite quickly as
$n$ increased.
\section{Conclusion}
It is evident that none of the implementations achieved even half of the 
theoretical peak performance of a core.
To completely discuss every reason why this was the case would
undoubtedly require several textbooks and several years of lectures
on the subject. As a result, only a few will be suggested below, 
with the most important being inefficient cache usage. A minor note concerning
more efficient algorithms will be given. \\

An inefficient usage of cache is perhaps the most important cause of 
poor performance, as is evidenced in the
the plots of performance rate. Notice, for example, the
significant drop in the performance rate for
$C=C+AB$ for the Mac near $n=500$.  
After all, for $n=500$, three $500$ double precision
matrices are being stored and operated on. Since 
$C$ is accessed sequentially and $A$ and $B$ are not,
the majority of cache misses are caused by $A$ and $B$. 
Note that these two matrices take 
$2\times 500\times 500\times 8\text{ bytes}$ or $4 MB$,
which is precisely the size of the shared L3 cache on the Mac's 
CPU (L1 and L2 are 
comparatively small, amounting to around a third of a megabyte).
As a result, it seems reasonable that the grand decrease in 
performance rate around $n=500$ on the Mac is at least in part
due to wasteful usage of the cache.
Usage of techniques such as 
tiling or blocking, as discussed in class, 
could certainly put the memory hierarchy to better use. 
Perhaps even exploiting certain compiler optimizations 
might stand to reason. Simply parallelizing the code might even benefit, 
as locality of reference would be increased as the matrix is 
necessarily blocked or tiled, just on different processors.\\

Finally, perhaps better algorithms could be investigated. The 
na\"ive approach implemented above is quite intuitive, 
but just because something
is easy to program or understand does not mean it will 
behave efficiently on a machine. For example, Strassen's or 
related algorithms for a matrix multiply might be employed to 
reduce the number of operations for large (and sometimes extremely so) 
$n$. Research has also
been done on preprocessing to optimize matrix-vector multiplication.
Perhaps further investigation and application of these ideas 
would benefit the implementation. Nevertheless, the speedups that
more efficient algorithms such as Strassen's provide pale in comparison
to algorithms designed to simply make better use of the cache.\\

In closing, this assignment was certainly informative, as it 
demonstrated the improvement of processor design over the past decade
along with the importance of writing performance algorithms to
make optimal usage of the cache.
Also, it is even more surprising that
the older processor on the Dell performed almost comparable to the
Mac with "that really great i7 processor you have to have"
when it came to matrix-matrix multiplication.
Finally, studying about advances and better ways to implement 
these algorithms provided the author with excellent topics for 
further study. It is his hope that he is able to study them in
detail and apply these concepts in his research.
\end{document}
