#include "matrixUtils.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

double randInRange(double a, double b){
	return a+(rand()/(RAND_MAX/(b-a)));
}

double d2norm(double* x, int n){
	double sum = 0;
	for(int i = 0;i < n; ++i){
		sum += x[i]*x[i];
	}
	return sqrt(sum);	
}

void dAx(double* A, double* x, double* b, int m, int n){
	int j;
	for(int i = 0; i < m; ++i){
		for(j = 0; j < n; ++j){
			b[i] += A[i*n+j]*x[j];
		}
	}	
}

void dAB(double* A, double* B, double* C, int m, int k, int n){
	int j, l;
	double sum = 0;
	for(int i = 0; i < m; ++i){
		for(j = 0; j < n; ++j){
			sum = 0;
			for(l = 0; l < k; ++l){
				sum += A[i*k+l]*B[l*n+j];
			}
			C[i*n+j] += sum;
		}
	}
}

void printVector(double* x, long n){
	for(int i = 0; i < n; i++){
		printf("%f\n",x[i]);
	}
}

void printMatrix(double* A, long m, long n){
	for(int i = 0; i < m; i++){
		for(int j = 0; j < n; j++){
			printf("%f\t",A[i*n+j]);
		}
		printf("\n");
	}
}
