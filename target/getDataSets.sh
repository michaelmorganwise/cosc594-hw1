#!/bin/bash
rm d2nrm.dat dAxpy.dat dABpC.dat
for((i=10;i<=1000;i+=10))
do
	echo Generating data for n = $i
	./d2nrm $i >> d2nrm.dat
	./dAxpy $i >> dAxpy.dat
	./dABpC $i >> dABpC.dat
done
